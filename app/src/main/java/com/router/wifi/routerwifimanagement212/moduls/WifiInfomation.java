package com.router.wifi.routerwifimanagement212.moduls;

public class WifiInfomation {
    public String sID;
    public String sConnect;
    public String sMode;
    public String sIP;
    public String sDNS1;
    public String sDNS2;
    public String sGetaway;
    public String sNetMask;

    public WifiInfomation(String sConnect, String sMode, String sIP, String sDNS1, String sDNS2, String sGetaway, String sNetMask) {
        this.sConnect = sConnect;
        this.sMode = sMode;
        this.sIP = sIP;
        this.sDNS1 = sDNS1;
        this.sDNS2 = sDNS2;
        this.sGetaway = sGetaway;
        this.sNetMask = sNetMask;
    }

    public WifiInfomation() {

    }
}
