package com.router.wifi.routerwifimanagement212.datebases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.router.wifi.routerwifimanagement212.moduls.WifiInfomation;

import java.util.ArrayList;

public class ManageWiFiConnection extends SQLiteOpenHelper {
    private static final String DATA_NAME = "Router_Wifi";
    private static final int VERSION = 1;

    private static final String COLUMN_WIFI_ID = "Wifi_Id";
    private static final String COLUMN_WIFI_CONNECT = "Wifi_Connect";
    private static final String COLUMN_WIFI_MODE = "Wifi_Mode";
    private static final String COLUMN_WIFI_IP = "WIFI_IP";
    private static final String COLUMN_WIFI_DNS1 = "Wifi_DNS1";
    private static final String COLUMN_WIFI_DNS2 = "Wifi_DNS2";
    private static final String COLUMN_WIFI_GETAWAY = "Wifi_Getaway";
    private static final String COLUMN_WIFI_NET_MASK = "Wifi_Net_Mask";

    public ManageWiFiConnection(Context context) {
        super(context, DATA_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String createData = "CREATE TABLE " + DATA_NAME + " ( " + COLUMN_WIFI_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + COLUMN_WIFI_CONNECT + " TEXT ," + COLUMN_WIFI_MODE + " TEXT , " + COLUMN_WIFI_IP + " TEXT , "
                + COLUMN_WIFI_DNS1 + " TEXT , " + COLUMN_WIFI_DNS2 + " TEXT , " + COLUMN_WIFI_GETAWAY + " TEXT , "
                + COLUMN_WIFI_NET_MASK + " TEXT " + " ) ";
        db.execSQL(createData);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DATA_NAME);
        onCreate(db);
    }

    public ArrayList<WifiInfomation> getAllConnectWifi() {
        ArrayList<WifiInfomation> Wifi = new ArrayList<WifiInfomation>();
        String selectQuery = "SELECT  * FROM " + DATA_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                WifiInfomation mWifi = new WifiInfomation();
                mWifi.sID = String.valueOf(cursor.getString(0));
                mWifi.sConnect = cursor.getString(1);
                mWifi.sMode = cursor.getString(2);
                mWifi.sIP = cursor.getString(3);
                mWifi.sDNS1 = cursor.getString(4);
                mWifi.sDNS2 = cursor.getString(5);
                mWifi.sGetaway = cursor.getString(6);
                mWifi.sNetMask = cursor.getString(7);
                Wifi.add(mWifi);
            } while (cursor.moveToNext());
        }
        return Wifi;
    }

    public boolean addItem(WifiInfomation wifi) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_WIFI_CONNECT, wifi.sConnect);
        values.put(COLUMN_WIFI_MODE, wifi.sMode);
        values.put(COLUMN_WIFI_IP, wifi.sIP);
        values.put(COLUMN_WIFI_DNS1, wifi.sDNS1);
        values.put(COLUMN_WIFI_DNS2, wifi.sDNS2);
        values.put(COLUMN_WIFI_GETAWAY, wifi.sGetaway);
        values.put(COLUMN_WIFI_NET_MASK, wifi.sNetMask);
        long kt = db.insert(DATA_NAME, null, values);
        if (kt != 0) {
            return true;
        }
        return false;
    }

    public void deleteItem(WifiInfomation wifi) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DATA_NAME, COLUMN_WIFI_ID + " = ?",
                new String[]{String.valueOf(wifi.sID)});
        db.close();
    }

    public SQLiteDatabase open() {
        return this.getWritableDatabase();
    }
}
