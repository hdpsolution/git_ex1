package com.router.wifi.routerwifimanagement212.adapters;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListPopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.router.wifi.routerwifimanagement212.R;
import com.router.wifi.routerwifimanagement212.datebases.ManageWiFiConnection;
import com.router.wifi.routerwifimanagement212.moduls.WifiInfomation;

import java.util.ArrayList;

public class HistoryConnectAdapter extends RecyclerView.Adapter<HistoryConnectAdapter.RecyclerViewHolder> {
    private ArrayList<WifiInfomation> arrWifi;
    private Context context;
    private TextView txtCancel, txtOK;

    public HistoryConnectAdapter(ArrayList<WifiInfomation> arrWifi, Context context) {
        this.arrWifi = arrWifi;
        this.context = context;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_history_view, viewGroup, false);
        return new HistoryConnectAdapter.RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder viewHolder, final int position) {
        final WifiInfomation mWifi = arrWifi.get(position);
        viewHolder.txtConnect.setText(mWifi.sConnect);
        viewHolder.txtMode.setText(mWifi.sMode);
        viewHolder.txtIP.setText(mWifi.sIP);
        viewHolder.txtDNS1.setText(mWifi.sDNS1);
        viewHolder.txtDNS2.setText(mWifi.sDNS2);
        viewHolder.txtGetaway.setText(mWifi.sGetaway);
        viewHolder.txtNetMask.setText(mWifi.sNetMask);

        viewHolder.llView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                PopupMenu popup = new PopupMenu(context, v);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.copy:
                                ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                                ClipData clip = android.content.ClipData.newPlainText("Clip",
                                        context.getResources().getString(R.string.title_connect_to) + ":" + mWifi.sConnect + "\n"
                                                + context.getResources().getString(R.string.title_mode) + ":" + mWifi.sMode + "\n"
                                                + context.getResources().getString(R.string.title_ip) + ":" + mWifi.sIP + "\n"
                                                + context.getResources().getString(R.string.title_dns1) + ":" + mWifi.sDNS1 + "\n"
                                                + context.getResources().getString(R.string.title_dns2) + ":" + mWifi.sDNS2 + "\n"
                                                + context.getResources().getString(R.string.title_gateway) + ":" + mWifi.sGetaway + "\n"
                                                + context.getResources().getString(R.string.title_net_mask) + ":" + mWifi.sNetMask);
                                clipboard.setPrimaryClip(clip);
                                Toast.makeText(context, context.getResources().getString(R.string.copy_sucess), Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.share:
                                Bitmap screenshot = Bitmap.createBitmap(viewHolder.llView.getWidth(), viewHolder.llView.getHeight(), Bitmap.Config.RGB_565);
                                viewHolder.llView.draw(new Canvas(screenshot));
                                String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), screenshot, "title", null);
                                Uri screenshotUri = Uri.parse(path);
                                Intent intentShare = new Intent(Intent.ACTION_SEND);
                                intentShare.setType("image/*");
                                intentShare.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intentShare.putExtra(Intent.EXTRA_STREAM, screenshotUri);
                                intentShare.putExtra(Intent.EXTRA_SUBJECT, context.getResources().getString(R.string.app_name));
                                context.startActivity(Intent.createChooser(intentShare, context.getResources().getString(R.string.share_with)));
                                break;

                            case R.id.delete:
                                final Dialog mDialog = new Dialog(context);
                                mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                mDialog.setContentView(R.layout.dialog_confirm);
                                mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                txtCancel = mDialog.findViewById(R.id.txtCancelConfirm);
                                txtOK = mDialog.findViewById(R.id.txtOKConfirm);

                                txtCancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mDialog.dismiss();
                                    }
                                });

                                txtOK.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        ManageWiFiConnection db = new ManageWiFiConnection(context);
                                        db.deleteItem(arrWifi.get(position));
                                        arrWifi.remove(arrWifi.get(position));
                                        notifyDataSetChanged();
                                        mDialog.dismiss();
                                    }
                                });

                                mDialog.show();
                                break;


                        }
                        return false;
                    }
                });
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.options_menu, popup.getMenu());

                popup.setGravity(Gravity.RIGHT);


                popup.show();

                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrWifi.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout llView;
        private TextView txtConnect, txtMode, txtIP, txtDNS1, txtDNS2, txtGetaway, txtNetMask;

        public RecyclerViewHolder(View mView) {
            super(mView);
            llView = mView.findViewById(R.id.llView);
            txtConnect = mView.findViewById(R.id.txtHistoryConnect);
            txtMode = mView.findViewById(R.id.txtHistoryMode);
            txtIP = mView.findViewById(R.id.txtHistoryIP);
            txtDNS1 = mView.findViewById(R.id.txtHistoryDNS1);
            txtDNS2 = mView.findViewById(R.id.txtHistoryDNS2);
            txtGetaway = mView.findViewById(R.id.txtHistoryGateway);
            txtNetMask = mView.findViewById(R.id.txtHistoryNetMask);
        }
    }
}
