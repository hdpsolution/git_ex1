package com.router.wifi.routerwifimanagement212.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

public class KillAppService extends Service {
    Context mContext;

    public KillAppService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void sendMessageToActivity() {
        Intent intent = new Intent("KILL_APP");
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        sendMessageToActivity();
        stopSelf();
    }
}
