package com.router.wifi.routerwifimanagement212;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.router.wifi.routerwifimanagement212.moduls.EventKeys;

import at.grabner.circleprogress.CircleProgressView;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    CircleProgressView mProgress;
    LinearLayout llViewLoad;
    NetworkInfo mWifi;
    WebView mWebView;
    String sIPWifi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();
        checkConnectWFView();
        loadWebView();
    }

    private void checkConnectWFView() {
        Thread mThread = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loadView();
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        mThread.start();
        mProgress.spin();
    }

    private void loadWebView() {
//        mWebView.setListener(this, this);
//        mWebView.setGeolocationEnabled(false);
//        mWebView.setMixedContentAllowed(true);
//        mWebView.setCookiesEnabled(false);
//        mWebView.setThirdPartyCookiesEnabled(false);
//        mWebView.setDesktopMode(true);

        WebSettings webSetting = mWebView.getSettings();
        webSetting.setJavaScriptEnabled(true);
        webSetting.setDisplayZoomControls(true);
        webSetting.setLoadWithOverviewMode(true);
        webSetting.setUseWideViewPort(true);
        webSetting.setBuiltInZoomControls(true);
        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
                super.onReceivedSslError(view, handler, error);
            }


            @Override
            public void onPageFinished(WebView view, String url) {

            }

        });
        mWebView.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);

            }

        });
        //  mWebView.addHttpHeader("X-Requested-With", "");

        mWebView.loadUrl("http://" + sIPWifi + "/");


    }

    private void loadView() {
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (mWifi.isConnected()) {
            llViewLoad.setVisibility(View.GONE);
            mWebView.setVisibility(View.VISIBLE);
        } else {
            llViewLoad.setVisibility(View.VISIBLE);
            mWebView.setVisibility(View.GONE);
        }
    }

    private void init() {
        mProgress = findViewById(R.id.circleView);
        llViewLoad = findViewById(R.id.llViewLoad);
        mWebView = findViewById(R.id.wbViewManagerWifi);

        sIPWifi = getIntent().getStringExtra(EventKeys.IP_WIFI);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
        }
    }

//    @Override
//    public void onPageStarted(String url, Bitmap favicon) {
//
//    }
//
//    @Override
//    public void onPageFinished(String url) {
//
//    }
//
//    @Override
//    public void onPageError(int errorCode, String description, String failingUrl) {
//
//    }
//
//    @Override
//    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {
//
//    }
//
//    @Override
//    public void onExternalPageRequest(String url) {
//
//    }
}
