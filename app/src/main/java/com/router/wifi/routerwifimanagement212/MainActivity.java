package com.router.wifi.routerwifimanagement212;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.text.format.Formatter;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.MobileAds;
import com.router.wifi.routerwifimanagement212.ads.Idelegate;
import com.router.wifi.routerwifimanagement212.ads.MyAdmobController;
import com.router.wifi.routerwifimanagement212.ads.MyBaseActivityWithAds;
import com.router.wifi.routerwifimanagement212.datebases.ManageWiFiConnection;
import com.router.wifi.routerwifimanagement212.moduls.EventKeys;
import com.router.wifi.routerwifimanagement212.moduls.WifiInfomation;
import com.router.wifi.routerwifimanagement212.services.KillAppService;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class MainActivity extends MyBaseActivityWithAds
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    Dialog dialogInfo, mDialog;
    TextView txtAuthor, txtPrivacy, txtEmail, txtOk, txtOKWarning;
    Toolbar toolbar;
    DrawerLayout mDrawer;
    NavigationView mNavigationView;

    TextView txtConnect, txtMode, txtIP, txtDNS1, txtDNS2, txtPort, txtNetMask, txtIP2;
    String sConnect, sMode, sIP, sDNS1, sDNS2, sPort, sNetMask;
    ManageWiFiConnection mManageWifi;
    NetworkInfo mWifi;

    String sGetway;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MobileAds.initialize(this, MyAdmobController.getApplicationAdsId(getApplicationContext()));

        MyAdmobController.setTypeQuangCao(getApplicationContext());

        MyAdmobController.XulyQCFull(this);

        MyAdmobController.listenNetworkChangeToRequestAdsFull(this);


        WarningConnectWF();
        setContentView(R.layout.activity_main);

        init();
        setSupportActionBar(toolbar);


        Thread mThread = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                InfoWiFi();
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        mThread.start();
        checkpermission();
        startService();
        createMessage();
    }

    private void InfoWiFi() {
        checkWiFi();

        if (mWifi.isConnected()) {
            final WifiManager manager = (WifiManager) super.getApplicationContext().getSystemService(WIFI_SERVICE);
            final DhcpInfo dhcp = manager.getDhcpInfo();
            WifiInfo myWifiInfo = manager.getConnectionInfo();


            Enumeration<NetworkInterface> interfaces = null;
            try {
                interfaces = NetworkInterface.getNetworkInterfaces();
                while (interfaces.hasMoreElements()) {
                    NetworkInterface networkInterface = interfaces.nextElement();
                    if (networkInterface.isLoopback())
                        continue;

                    for (InterfaceAddress interfaceAddress :
                            networkInterface.getInterfaceAddresses()) {
                        InetAddress address = interfaceAddress.getAddress();
                        if (address instanceof Inet4Address) {
                            sIP = Formatter.formatIpAddress(dhcp.ipAddress) + "/" + interfaceAddress.getNetworkPrefixLength() + "(wlan0)";
                            txtIP.setText(sIP);
                        }
                    }
                }
            } catch (SocketException e) {
                e.printStackTrace();
            }
            sConnect = findSSIDForWifiInfo(manager, myWifiInfo).replace("\"", "");
            sMode = "WiFi (" + myWifiInfo.getLinkSpeed() + " Mbps)";
            sDNS1 = Formatter.formatIpAddress(dhcp.dns1);
            sDNS2 = Formatter.formatIpAddress(dhcp.dns2);
            sPort = Formatter.formatIpAddress(dhcp.gateway);
            sNetMask = Formatter.formatIpAddress(dhcp.netmask);

            txtConnect.setText(sConnect);
            txtMode.setText(sMode);
            txtDNS1.setText(sDNS1);
            txtDNS2.setText(sDNS2);
            txtPort.setText(sPort);
            txtNetMask.setText(sNetMask);
            sGetway = Formatter.formatIpAddress(dhcp.gateway);
            txtIP2.setText(Formatter.formatIpAddress(dhcp.gateway));
        } else {
            txtConnect.setText("n/a");
            txtMode.setText("n/a");
            txtDNS1.setText("n/a");
            txtDNS2.setText("n/a");
            txtPort.setText("n/a");
            txtNetMask.setText("n/a");
            txtIP2.setText("n/a");
            txtIP.setText("n/a");
        }
    }

    private void checkWiFi() {
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
    }

    private void startService() {
        startService(new Intent(this, KillAppService.class));
    }

    public String findSSIDForWifiInfo(WifiManager manager, WifiInfo wifiInfo) {

        List<WifiConfiguration> listOfConfigurations = manager.getConfiguredNetworks();

        for (int index = 0; index < listOfConfigurations.size(); index++) {
            WifiConfiguration configuration = listOfConfigurations.get(index);
            if (configuration.networkId == wifiInfo.getNetworkId()) {
                return configuration.SSID;
            }
        }

        return null;
    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);
        mDrawer = findViewById(R.id.drawer_layout);
        mDrawer = findViewById(R.id.drawer_layout);
        mNavigationView = findViewById(R.id.nav_view);
        txtIP = findViewById(R.id.txtIPWifi);
        txtIP2 = findViewById(R.id.txtIPWifi2);
        txtConnect = findViewById(R.id.txtNameConnect);
        txtMode = findViewById(R.id.txtModeSpeed);
        txtDNS1 = findViewById(R.id.txtDNS1Wifi);
        txtDNS2 = findViewById(R.id.txtDNS2Wifi);
        txtPort = findViewById(R.id.txtGatewayWifi);
        txtNetMask = findViewById(R.id.txtNetMaskWifi);

        mNavigationView.setNavigationItemSelectedListener(this);
        mManageWifi = new ManageWiFiConnection(this);
    }

    private void createMessage() {
        LocalBroadcastManager.getInstance(getApplication()).registerReceiver(
                mMessageReceiver, new IntentFilter("KILL_APP"));
    }

    private void WarningConnectWF() {
        checkWiFi();
        if (!mWifi.isConnected()) {
            dialogWarningConnectWF();
        }
    }

    private void dialogWarningConnectWF() {
        mDialog = new Dialog(this);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_check_wifi);
        mDialog.setCancelable(false);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        txtOKWarning = mDialog.findViewById(R.id.txtOkWarning);
        txtOKWarning.setOnClickListener(this);
        mDialog.show();
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            killapp();
        }
    };

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
            killapp();

            MyAdmobController.releaseQC_Callbacks();

            finish();
        }
    }

    private void killapp() {
        if (mWifi.isConnected()) {
            mManageWifi.addItem(new WifiInfomation(sConnect, sMode, sIP, sDNS1, sDNS2, sPort, sNetMask));

        }
    }

    private void share() {
        Intent intentShare = new Intent(Intent.ACTION_SEND);
        intentShare.setType("text/plain");
        intentShare.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        intentShare.putExtra(Intent.EXTRA_TEXT, getString(R.string.title_dowload)
                + "https://play.google.com/store/apps/details?id=" + getPackageName());
        startActivity(Intent.createChooser(intentShare, getString(R.string.share_with)));
    }

    private void rate() {
        Intent intentRate = new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName()));
        startActivity(intentRate);
    }

    private void onCreateDialogInfo() {
        dialogInfo = new Dialog(MainActivity.this);
        dialogInfo.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogInfo.setContentView(R.layout.dialog_info_app);
        txtAuthor = dialogInfo.findViewById(R.id.txtAuthor);
        txtPrivacy = dialogInfo.findViewById(R.id.txtPrivacy);
        txtEmail = dialogInfo.findViewById(R.id.txtEmail);
        txtOk = dialogInfo.findViewById(R.id.btnOK);
        dialogInfo.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogInfo.cancel();
            }
        });

        txtAuthor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentRate = new Intent(Intent.ACTION_VIEW, Uri.parse("https://hdpsolutions.com/"));
                startActivity(intentRate);
            }
        });

        txtPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentRate = new Intent(Intent.ACTION_VIEW, Uri.parse("http://hdpsolution.com/privacy_policy/CameraApps/PrivacyPolicyCameraApps.htm"));
                startActivity(intentRate);
            }
        });

        txtEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                String[] recipients = {getString(R.string.title_link_email)};
                intent.putExtra(Intent.EXTRA_EMAIL, recipients);
                intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.title_feedback) + " " + getString(R.string.app_name));
                intent.setType("text/html");
                startActivity(Intent.createChooser(intent, getString(R.string.title_choser_email)));
            }
        });
        dialogInfo.show();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_history) {

            MyAdmobController.showAdsFullBeforeDoAction(new Idelegate() {
                @Override
                public void callBack(Object value, int where) {
                    startActivity(new Intent(getApplicationContext(), HistoryActivity.class));
                }
            });

        } else if (id == R.id.nav_share) {
            share();
        } else if (id == R.id.nav_rate) {
            rate();
        } else if (id == R.id.nav_about) {
            onCreateDialogInfo();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void checkpermission() {
        String[] params = null;
        String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        String readExternalStorage = Manifest.permission.READ_EXTERNAL_STORAGE;


        int hasWriteExternalStoragePermission = ActivityCompat.checkSelfPermission(this, writeExternalStorage);
        int hasReadExternalStoragePermission = ActivityCompat.checkSelfPermission(this, readExternalStorage);


        List<String> permissions = new ArrayList<>();

        if (hasWriteExternalStoragePermission != PackageManager.PERMISSION_GRANTED)
            permissions.add(writeExternalStorage);
        if (hasReadExternalStoragePermission != PackageManager.PERMISSION_GRANTED)
            permissions.add(readExternalStorage);

        if (!permissions.isEmpty()) {
            params = permissions.toArray(new String[permissions.size()]);
        }
        if (params != null && params.length > 0) {
            ActivityCompat.requestPermissions(MainActivity.this, params, 121);
        } else {
        }
    }

    @Override
    public void onClick(View mView) {
        switch (mView.getId()) {
            case R.id.navHeard:
                mDrawer.openDrawer(Gravity.START);
                break;

            case R.id.imgWifi:
                startActivity(new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK));
                break;

            case R.id.txtLogin:

                MyAdmobController.showAdsFullBeforeDoAction(new Idelegate() {
                    @Override
                    public void callBack(Object value, int where) {
                        Intent intentLogin = new Intent(getApplicationContext(), LoginActivity.class);
                        intentLogin.putExtra(EventKeys.IP_WIFI, sGetway);
                        startActivity(intentLogin);
                    }
                });

                break;

            case R.id.txtOkWarning:

                try {
                    if (mDialog.isShowing()) {
                        mDialog.dismiss();
                    }
                } catch (Exception e) {

                }

                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

    }
}
