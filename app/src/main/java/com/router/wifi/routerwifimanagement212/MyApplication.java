package com.router.wifi.routerwifimanagement212;

import android.app.Application;
import android.os.Build;
import android.os.StrictMode;

import com.facebook.ads.AudienceNetworkAds;

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            builder.detectFileUriExposure();
            StrictMode.setVmPolicy(builder.build());
        }

        AudienceNetworkAds.initialize(getApplicationContext());

    }
}
