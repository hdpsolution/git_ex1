package com.router.wifi.routerwifimanagement212;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.router.wifi.routerwifimanagement212.adapters.HistoryConnectAdapter;
import com.router.wifi.routerwifimanagement212.ads.MyBaseActivityWithAds;
import com.router.wifi.routerwifimanagement212.datebases.ManageWiFiConnection;
import com.router.wifi.routerwifimanagement212.moduls.WifiInfomation;

import java.util.ArrayList;

public class HistoryActivity extends MyBaseActivityWithAds implements View.OnClickListener {
    RecyclerView mRecycHistory;
    HistoryConnectAdapter mAdapter;
    ArrayList<WifiInfomation> arrConnect = new ArrayList<>();
    ManageWiFiConnection mManageWifi;

    @Override
    protected void onCreate(Bundle savedInstanceState) { //
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        init();
        loadHistory();
    }

    private void init() {
        mRecycHistory = findViewById(R.id.recycHistory);
    }

    private void loadHistory() {

        mManageWifi = new ManageWiFiConnection(this);
        mManageWifi.open();

        arrConnect = mManageWifi.getAllConnectWifi();
        mAdapter = new HistoryConnectAdapter(arrConnect, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecycHistory.setLayoutManager(layoutManager);
        mRecycHistory.setAdapter(mAdapter);

    }

    @Override
    public void onClick(View v) {
        onBackPressed();
    }
}
